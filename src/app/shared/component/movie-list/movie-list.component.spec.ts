import { expect, describe, beforeEach, test, jest } from '@jest/globals';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatGridListHarness } from '@angular/material/grid-list/testing';
import { MatGridListModule } from '@angular/material/grid-list';
import { MovieListComponent } from './movie-list.component';
import { By } from '@angular/platform-browser';
import { EmptyResultComponent } from './empty-result/empty-result.component';
import { MovieCardComponent } from './movie-card/movie-card.component';
import { Movie } from '@App/app/shared/model/movie.model';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { RouterTestingModule } from '@angular/router/testing';
import { MovieComponent } from '@App/app/page/movie/movie.component';
import { ChangeDetectionStrategy } from '@angular/core';
import { CustomPipeModule } from '../../pipe/pipe.module';

jest.mock('@App/app/shared/model/movie.model');

const LIST_SIZE = 20;
const ZERO = 0;
describe('MovieListComponent', () => {
  let component: MovieListComponent;
  let fixture: ComponentFixture<MovieListComponent>;
  const movies = new Array(LIST_SIZE).fill(new Movie());
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ 
        MovieListComponent,
        EmptyResultComponent, 
        MovieCardComponent 
      ],
      imports: [
        MatGridListModule,
        MatCardModule,
        MatRippleModule,
        CustomPipeModule,
        RouterTestingModule.withRoutes([
          { path: 'movie', component: MovieComponent },
        ]) 
      ]
    })
    .overrideComponent(MovieListComponent, {
      set: { changeDetection: ChangeDetectionStrategy.Default }
    })
    .compileComponents();

    fixture = TestBed.createComponent(MovieListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('with empty state', () => {
    const emptyState = fixture.debugElement.query(By.css('empty-result'));
    expect(emptyState).toBeTruthy();
  });

  it('sould render the list with 20 elements', async () => {
    movies[ZERO].poster_path = '';
    component.movies = movies;
    await fixture.whenStable();
    const loader = TestbedHarnessEnvironment.loader(fixture);
    const grid = await loader.getHarness(MatGridListHarness);
    expect(grid).toBeTruthy();
    let list = fixture.debugElement.queryAll(By.css('movie-card'));
    expect(list.length).toBe(LIST_SIZE);
  });
});
