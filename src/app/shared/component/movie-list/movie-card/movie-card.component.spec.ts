import { expect, describe, beforeEach, test } from '@jest/globals';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { RouterTestingModule } from '@angular/router/testing';
import { MatCardHarness } from '@angular/material/card/testing';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';

import { Movie } from '@App/app/shared/model/movie.model';
import { MovieCardComponent } from './movie-card.component';
import { ChangeDetectionStrategy } from '@angular/core';
import { MovieComponent } from '@App/app/page/movie/movie.component';
import { By } from '@angular/platform-browser';
import { CustomPipeModule } from '@App/app/shared/pipe/pipe.module';

jest.mock('@App/app/shared/model/movie.model');

const EMPTY_STRING = '';

describe('MovieCardComponent', () => {
  let component: MovieCardComponent;
  let fixture: ComponentFixture<MovieCardComponent>;
  const mockMovie = new Movie();
  beforeEach(async () => {

    await TestBed.configureTestingModule({
      declarations: [ MovieCardComponent ],
      imports: [
        MatCardModule, 
        MatRippleModule,
        CustomPipeModule,
        RouterTestingModule.withRoutes([
          { path: 'movie', component: MovieComponent },
        ]) 
      ]
    })
    .overrideComponent(MovieCardComponent, {
      set: { changeDetection: ChangeDetectionStrategy.Default }
    })
    .compileComponents();

    fixture = TestBed.createComponent(MovieCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  it('empty movie should create a empty card', async () => {
    const loader = TestbedHarnessEnvironment.loader(fixture);
    const card = await loader.getHarness(MatCardHarness);
    const content = await card.getText();
    expect(content).toBe(EMPTY_STRING);
  });

  it('using the data from mockMovie should render the component', async () => {
    const mockedUrl = `/movie,${mockMovie.id}`;
    const mockedImageSrc = `https://image.tmdb.org/t/p/w185${mockMovie.poster_path}`;
    component.movie = mockMovie;
    await fixture.whenStable();
    const loader = TestbedHarnessEnvironment.loader(fixture);
    const card = await loader.getHarness(MatCardHarness);
    const title = await card.getTitleText();
    expect(title).toBe(mockMovie.title);
    const subtitle = await card.getSubtitleText();
    expect(subtitle).toBe('14 DEC 2022');
    const image = fixture.debugElement.query(By.css('img')).nativeElement;
    expect(image.getAttribute('src')).toBe(mockedImageSrc);
    const link = fixture.debugElement.query(By.css('a')).nativeElement;
    expect(link.getAttribute('ng-reflect-router-link')).toBe(mockedUrl);
  });

  it('using null in poster_path src should render the poster place holder', async () => {
    mockMovie.poster_path = '';
    const mockedImageSrc = component.posterPlaceHolderUrl;
    component.movie = mockMovie;
    await fixture.whenStable();
    const loader = TestbedHarnessEnvironment.loader(fixture);
    const card = await loader.getHarness(MatCardHarness);
    const title = await card.getTitleText();
    expect(title).toBe(mockMovie.title);
    const image = fixture.debugElement.query(By.css('img')).nativeElement;
    expect(image.getAttribute('src')).toBe(mockedImageSrc);
  });
});
