import { Component, Input } from '@angular/core';
import { environment } from '@App/environments/environment';
import { Movie } from '@App/app/shared/model/movie.model';
const POSTER_SIZE = 'w185';
@Component({
  selector: 'movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.sass']
})
export class MovieCardComponent {
  @Input() movie!: Movie;
  imgBaseUrl = environment.api.posterBaseUrl;
  posterPlaceHolderUrl = environment.api.posterPlaceHolder;

  getPosterURL() {
    if (this.movie.poster_path == null || this.movie.poster_path == '') {
      return this.posterPlaceHolderUrl;
    }
    return this.imgBaseUrl + POSTER_SIZE + this.movie.poster_path;
  }
}
