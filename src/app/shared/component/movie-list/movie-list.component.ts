import { Component, Input } from '@angular/core';
import { Movie } from '@App/app/shared/model/movie.model';

@Component({
  selector: 'movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.sass']
})
export class MovieListComponent {
  @Input()
  cols = 2;
  @Input()
  movies: Movie[] = [];

  trackByMovie(index: number, movie: Movie) {
    return movie.id;
  }
}
