import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatRippleModule } from '@angular/material/core';
import { RouterModule } from '@angular/router';

import { MovieCardComponent } from './movie-card/movie-card.component';
import { MovieListComponent } from './movie-list.component';
import { EmptyResultComponent } from './empty-result/empty-result.component';
import { CustomPipeModule } from '../../pipe/pipe.module';

@NgModule({
  declarations: [
    MovieListComponent,
    MovieCardComponent,
    EmptyResultComponent
  ],
  imports: [
    CommonModule,
    MatGridListModule,
    MatCardModule,
    MatRippleModule,
    RouterModule,
    CustomPipeModule
  ],
  exports: [
    MovieListComponent,
    MovieCardComponent
  ],
})
export class MovieListModule { }
