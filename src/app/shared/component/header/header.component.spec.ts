import { ComponentFixture, TestBed } from '@angular/core/testing';
import { expect, test, describe } from '@jest/globals';
import { By } from '@angular/platform-browser';

import { HeaderComponent } from './header.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HomeComponent } from '@App/app/page/home/home.component';
/* para testar router é preciso usar RouterTestingModule 
  https://angular.io/api/router/testing/RouterTestingModule
*/
const HOME_ROUTE = "/";
describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let title = 'Homem de Ferro Filmes';

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderComponent ],
      imports: [ 
        RouterTestingModule.withRoutes([
          { path: '', component: HomeComponent }, 
        ])
      ]
    })
    .compileComponents();    
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    component.title = title;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('the title should be rendered in header h2 a', () => {
    const result: HTMLElement = fixture.debugElement
      .query(By.css('header h2 a')).nativeElement;
    expect(result.innerHTML).toBe(title);
  });

  test('the link should lead to home', () => {
    const result: HTMLElement = fixture.debugElement
      .query(By.css('header h2 a')).nativeElement;
    expect(result.getAttribute('ng-reflect-router-link')).toBe(HOME_ROUTE);
  });

});
