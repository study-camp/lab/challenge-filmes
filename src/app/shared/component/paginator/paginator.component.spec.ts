import { expect, describe, beforeEach, test } from '@jest/globals';
import { ChangeDetectionStrategy, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatIconHarness } from '@angular/material/icon/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { PaginatorComponent } from './paginator.component';
import { HomeComponent } from '@App/app/page/home/home.component';
import { MatIconModule } from '@angular/material/icon';
const ZERO = 0
const FIRST = ZERO;
const ONE = 1;
const SECOND = ONE;
const TOTAL_PAGES = 4; 
const SIX = 6;
const CHANGED_PAGE_INDEX = 4;
const TOTAL_PAGES_LIMIT = 500;
const OVERBOUNDARY_TOTAL_PAGES = 501;
const MOBILE_PAGINATOR_SIZE = 3;
const MOBILE_TOTAL_BUTTONS = 7;
const DESKTOP_PAGINATOR_SIZE = 5;
const DESKTOP_TOTAL_BUTTONS = 9;

describe('PaginatorComponent', () => {
  let component: PaginatorComponent;
  let fixture: ComponentFixture<PaginatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginatorComponent ],
      imports: [ 
        MatButtonModule,
        MatIconModule,
        RouterTestingModule.withRoutes([
          { path: '', component: HomeComponent },
        ]) 
      ]
    })
    .overrideComponent(PaginatorComponent, {
      set: { changeDetection: ChangeDetectionStrategy.Default }
    })
    .compileComponents();

    fixture = TestBed.createComponent(PaginatorComponent);
    component = fixture.componentInstance;
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });
  // TestbedHarnessEnvironment é incompatível com jest aqui está sendo usado jasmine
  it('with only one button', async () => {
    const loader = TestbedHarnessEnvironment.loader(fixture);
    expect.assertions(1);
    loader.getAllHarnesses(MatButtonHarness).then(
      (response) => { 
        const buttonDebugElements = response;
        expect(buttonDebugElements.length).toBe(ONE);
      }
    );
  });

  it('with 0 pages create one button', async () => {
    const loader = TestbedHarnessEnvironment.loader(fixture);
    component.totalPages = ZERO;
    await fixture.whenStable();
    let buttonDebugElements = await loader.getAllHarnesses(MatButtonHarness);
    expect(buttonDebugElements.length).toBe(ONE);
  });

  it('the button have to be disabled', async () => {
    const loader = TestbedHarnessEnvironment.loader(fixture);
    let buttonDebugElements = await loader.getAllHarnesses(MatButtonHarness);
    await expect(buttonDebugElements[0].isDisabled()).resolves.toBeTruthy();
  });

  it('should lead to /?page=1', async () => {
    const loader = TestbedHarnessEnvironment.loader(fixture);
    let buttonDebugElements = await loader.getAllHarnesses(MatButtonHarness);
    const link = await buttonDebugElements[0].host();
    const attr = await link.getAttribute('href');
    expect(attr).toMatch(/page=1$/);
  });
  
  it('create a component with 6 buttons', async () => {
    const loader = TestbedHarnessEnvironment.loader(fixture);
    component.totalPages = TOTAL_PAGES;
    await fixture.whenStable();
    let buttonDebugElements = await loader.getAllHarnesses(MatButtonHarness);
    expect(buttonDebugElements.length).toBe(SIX);
  });

  it('from 6 buttons 4 have numeric title', async () => {
    const loader = TestbedHarnessEnvironment.loader(fixture);
    component.totalPages = TOTAL_PAGES;
    await fixture.whenStable();
    let buttonDebugElements = await loader.getAllHarnesses(
      MatButtonHarness.with({text: /\d/})
    );
    expect(buttonDebugElements.length).toBe(4);
  });

  it(`from 6 the last have title "Última"`, async () => {
    const loader = TestbedHarnessEnvironment.loader(fixture);
    component.totalPages = TOTAL_PAGES;
    await fixture.whenStable();
    let buttonDebugElements = await loader.getAllHarnesses(MatButtonHarness);
    const last = buttonDebugElements.at(buttonDebugElements.length - 1)!;
    await expect(last.getText()).resolves.toBe('Última');
    const link = await last.host();
    const attr = await link.getAttribute('href');
    expect(attr).toMatch(/page=4$/);
  });

  it(`from 1 before the last have a mat-icon`, async () => {
    const loader = TestbedHarnessEnvironment.loader(fixture);
    component.totalPages = TOTAL_PAGES;
    await fixture.whenStable();
    let buttonDebugElements = await loader.getAllHarnesses(MatButtonHarness);
    const penultimate = buttonDebugElements.at(buttonDebugElements.length - 2)!;
    const icon = await penultimate.getHarness(MatIconHarness);
    await expect(icon.getName()).resolves.toBe('navigate_next');
    const link = await penultimate.host();
    const attr = await link.getAttribute('href');
    expect(attr).toMatch(/page=2$/);
  });

  it(`changing the index the first should change from null to 1`, async () => {
    component.ngOnInit();
    expect(component.first).toBeNull();
    component.totalPages = TOTAL_PAGES;
    component.pageIndex = CHANGED_PAGE_INDEX;
    component.ngOnChanges({
      name: new SimpleChange(ONE, component.pageIndex, true)
    });
    expect(component.first).toBe(ONE);
  });

  it(`set maximum total_pages and set the paginatorSize to limit the amount of buttons on screen`, async () => {
    const loader = TestbedHarnessEnvironment.loader(fixture);
    component.totalPages = OVERBOUNDARY_TOTAL_PAGES;
    component.paginatorSize = MOBILE_PAGINATOR_SIZE;
    component.pageIndex = SIX;

    component.ngOnInit();
    await fixture.whenStable();

    let buttonDebugElements = await loader.getAllHarnesses(MatButtonHarness);
    expect(buttonDebugElements.length).toBe(MOBILE_TOTAL_BUTTONS);

    component.totalPages = OVERBOUNDARY_TOTAL_PAGES;
    component.paginatorSize = DESKTOP_PAGINATOR_SIZE;

    component.ngOnInit();
    await fixture.whenStable();

    buttonDebugElements = await loader.getAllHarnesses(MatButtonHarness);
    expect(buttonDebugElements.length).toBe(DESKTOP_TOTAL_BUTTONS);
  });

  it(`OVERSTEPING the total_pages boundary limit total_pages to 500`, async () => {
    component.ngOnInit();
    component.totalPages = OVERBOUNDARY_TOTAL_PAGES;
    component.ngOnChanges({
      name: new SimpleChange(TOTAL_PAGES, component.totalPages, true)
    });
    expect(component.last).toBe(TOTAL_PAGES_LIMIT);
  });

  it(`changing the index the first should change from null to 1`, async () => {
    component.ngOnInit();
    expect(component.first).toBeNull();
    component.totalPages = TOTAL_PAGES;
    component.pageIndex = CHANGED_PAGE_INDEX;
    component.ngOnChanges({
      name: new SimpleChange(1, component.pageIndex, true)
    });
    expect(component.first).toBe(1);
  });

  it(`with the index in the last page the first button should be "Primeira"`, async () => {
    const loader = TestbedHarnessEnvironment.loader(fixture);
    component.totalPages = TOTAL_PAGES;
    component.pageIndex = CHANGED_PAGE_INDEX;
    await fixture.whenStable();
    const buttonDebugElements = await loader.getAllHarnesses(MatButtonHarness);
    const first = buttonDebugElements[FIRST];
    await expect(first.getText()).resolves.toBe('Primeira');
  });

  it(`with the index in the last page the second button should have a mat-icon`, async () => {
    const loader = TestbedHarnessEnvironment.loader(fixture);
    component.totalPages = TOTAL_PAGES;
    component.pageIndex = CHANGED_PAGE_INDEX;
    await fixture.whenStable();
    const buttonDebugElements = await loader.getAllHarnesses(MatButtonHarness);
    const second = buttonDebugElements[SECOND];
    const icon = await second.getHarness(MatIconHarness);
    await expect(icon.getName()).resolves.toBe('navigate_before');
    const link = await second.host();
    const attr = await link.getAttribute('href');
    expect(attr).toMatch(/page=3$/);
  });
});
