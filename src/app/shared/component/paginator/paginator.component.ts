import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

const FIRST = 1;
const HALF = 2;
const DEFAULT_TOTAL = 1;
const DEFAULT_PAGE_SIZE = 20;
const DEFAULT_PAGINATOR_SIZE = 5;
const LIMIT_TMDB = 500;

@Component({
  selector: 'paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.sass']
})
export class PaginatorComponent implements OnInit, OnChanges{
  @Input() totalPages: number = DEFAULT_TOTAL;
  @Input() pageIndex: number = FIRST;
  @Input() paginatorSize: number = DEFAULT_PAGINATOR_SIZE;
  
  first: number | null = FIRST;
  last: number | null = FIRST;
  paginatorStart: number = FIRST ;
  pages: number[] = [FIRST];

  ngOnInit(): void {
    this.configure();
  }
  
  ngOnChanges(changes: SimpleChanges): void {
    this.configure();
  }
  configure() {
    this.totalPages = this.totalPages <= DEFAULT_TOTAL 
      ? DEFAULT_TOTAL 
      : this.totalPages;
    const lastPage = this.totalPages >= LIMIT_TMDB 
      ? LIMIT_TMDB 
      : this.totalPages;
    if (this.paginatorSize > lastPage) {
      this.paginatorSize = lastPage;
    }
    const halfSize = Math.floor(this.paginatorSize / HALF);
    const lastPages = lastPage - this.paginatorSize + FIRST;

    this.first = this.pageIndex > FIRST ? FIRST : null;
    this.last = this.pageIndex < lastPage? lastPage: null;
    this.paginatorStart = this.pageIndex - halfSize;

    if (this.paginatorStart > lastPages) {
      this.paginatorStart = lastPages;
    }

    if (this.paginatorStart < FIRST) {
      this.paginatorStart = FIRST;
    }
    
    this.pages = [...Array(this.paginatorSize).keys()]
      .map(value => value + this.paginatorStart);
  }

}
