import { FormatMinutesInHoursPipe } from './format-minutes-in-hours.pipe';

describe('Format Minutes In Hours Pipe', () => {
  test('create an instance', () => {
    const pipe = new FormatMinutesInHoursPipe();
    expect(pipe).toBeTruthy();
  });

  test('when 60 minutes return 1h 00m ', () => {
    const pipe = new FormatMinutesInHoursPipe();
    const minutes = 60;
    const oneHour = '1h 00m';
    expect(pipe.transform(minutes)).toBe(oneHour);
  });
  test('when 59 minutes return 0h 59m ', () => {
    const pipe = new FormatMinutesInHoursPipe();
    const minutes = 59;
    const fiftyNine = '0h 59m';
    expect(pipe.transform(minutes)).toBe(fiftyNine);
  });
  test('when negative minutes return 0h 00m ', () => {
    const pipe = new FormatMinutesInHoursPipe();
    const minutes = -1;
    const zero = '0h 00m';
    expect(pipe.transform(minutes)).toBe(zero);
  });
});
