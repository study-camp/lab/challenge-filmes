import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatMinutesInHoursPipe } from './format-minutes-in-hours.pipe';
import { ReplacePipe } from './replace.pipe';

@NgModule({
  declarations: [FormatMinutesInHoursPipe, ReplacePipe],
  imports: [
    CommonModule
  ],
  exports: [FormatMinutesInHoursPipe, ReplacePipe],
})
export class CustomPipeModule { }
