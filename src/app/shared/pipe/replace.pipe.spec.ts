import { expect, describe, test } from '@jest/globals';
import { ReplacePipe } from './replace.pipe';

describe('ReplacePipe', () => {
  test('create an instance', () => {
    const pipe = new ReplacePipe();
    expect(pipe).toBeTruthy();
  });

  test('should return null', () => {
    const pipe = new ReplacePipe();
    expect(pipe.transform(null)).toBeNull();
  });

  test(`shouldn't do nothing`, () => {
    const pipe = new ReplacePipe();
    expect(pipe.transform('abc')).toBe('abc');
  });

  test(`should return empty`, () => {
    const pipe = new ReplacePipe();
    expect(pipe.transform('abc', 'abc')).toBe('');
  });

  test(`should return "def"`, () => {
    const pipe = new ReplacePipe();
    expect(pipe.transform('abc', 'abc', 'def')).toBe('def');
  });

  test(`should replace only the first occurence of abc"`, () => {
    const pipe = new ReplacePipe();
    expect(pipe.transform('abcabc', 'abc', 'def')).toBe('defabc');
  });

  test(`using regex should return "1bc"`, () => {
    const pipe = new ReplacePipe();
    expect(pipe.transform('abc', /[a-z]/, '1')).toBe('1bc');
  });
});
