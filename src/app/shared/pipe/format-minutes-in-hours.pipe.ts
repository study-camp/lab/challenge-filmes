import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatMinutesInHours'
})
export class FormatMinutesInHoursPipe implements PipeTransform {
  transform(value: number): string {
    if (value <= 0) {
      return '0h 00m';
    }
    const hour = Math.floor(value / 60);
    const minutes = value % 60;
    return `${hour}h ${minutes.toString().padStart(2, '0')}m`;
  }
}
