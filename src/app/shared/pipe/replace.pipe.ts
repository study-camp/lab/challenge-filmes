import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replace'
})
export class ReplacePipe implements PipeTransform {
  transform(
    value: string | null, 
    pattern: string | RegExp = '', 
    replacement: string = ''
  ): string | null {
    return value?.replace(pattern, replacement) ?? value;
  }
}
