export interface ReleaseDatesResponse {
    id: number;
    results: ReleaseDateResult[];
}

export interface ReleaseDateResult {
    iso_3166_1: string;
    release_dates: ReleaseDate[];
}

export interface ReleaseDate {
    certification: string;
    iso_639_1: string;
    release_date: Date;
    type: number;
    note?: string;
}

export class ReleaseDatesResponse implements ReleaseDatesResponse {}
