const response: VideosResponse = JSON.parse(`{"id":76600,"results":[{"iso_639_1":"pt","iso_3166_1":"BR","name":"Trailer Legendado","key":"fMdb0nGsICE","site":"YouTube","size":1080,"type":"Trailer","official":true,"published_at":"2022-11-02T12:53:26.000Z","id":"63626e96a6a4c10082da772f"},{"iso_639_1":"pt","iso_3166_1":"BR","name":"Teaser Trailer Legendado","key":"-cmwzbZ072U","site":"YouTube","size":1080,"type":"Teaser","official":true,"published_at":"2022-05-09T14:26:09.000Z","id":"62792538d7a70a17a8d05c3a"}]}`);

export class VideosResponse implements VideosResponse {
    constructor() {
        return response;
    }
}