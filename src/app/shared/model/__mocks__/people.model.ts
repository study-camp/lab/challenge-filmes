const people: People = JSON.parse(`{ "adult": false, "gender": 2, "id": 85139, "known_for_department": "Acting", "name": "Nicholas Braun", "original_name": "Nicholas Braun", "popularity": 7.276, "profile_path": "/xunYVmn3IqICKiM7uY4s5T1BDty.jpg", "cast_id": 9, "character": "Dag Parker", "credit_id": "52fe4e55c3a368484e21b0a9", "order": 0 }`);

export class People {
    constructor() {
        return people;
    }
}
