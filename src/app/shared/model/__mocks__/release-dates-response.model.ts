const response: ReleaseDatesResponse = JSON.parse(`{"id":218784,"results":[{"iso_3166_1":"FR","release_dates":[{"certification":"","iso_639_1":"fr","release_date":"2016-02-15T00:00:00.000Z","type":4}]},{"iso_3166_1":"GB","release_dates":[{"certification":"15","iso_639_1":"","note":"","release_date":"2015-11-06T00:00:00.000Z","type":3}]},{"iso_3166_1":"DE","release_dates":[{"certification":"16","iso_639_1":"","release_date":"2016-03-24T00:00:00.000Z","type":3}]},{"iso_3166_1":"BR","release_dates":[{"certification":"16","iso_639_1":"pt","note":"","release_date":"2016-01-10T00:00:00.000Z","type":3}]},{"iso_3166_1":"US","release_dates":[{"certification":"R","iso_639_1":"en","note":"","release_date":"2015-10-30T00:00:00.000Z","type":2},{"certification":"","iso_639_1":"","release_date":"2016-02-09T00:00:00.000Z","type":5}]}]}`);

export class ReleaseDatesResponse implements ReleaseDatesResponse {
    constructor() {
        return response;
    }
}
