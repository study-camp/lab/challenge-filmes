const movie = {
    adult: false,
    backdrop_path: "/s16H6tpK2utvwDtzZ8Qy4qm5Emw.jpg",
    genre_ids: [1, 3, 4],
    id: 76600,
    original_language: "en",
    original_title: "Avatar: The Way of Water",
    overview: "12 anos depois de explorar Pandora e se juntar aos Na'vi, Jake Sully formou uma família com Neytiri e se estabeleceu entre os clãs do novo mundo. Porém, a paz não durará para sempre.",
    popularity: 10226.166,
    poster_path: "/mbYQLLluS651W89jO7MOZcLSCUw.jpg",
    release_date: new Date('2022-12-14T00:00:00'),
    title: "Avatar: O Caminho da Água",
    video: false,
    vote_average: 7.9,
    vote_count: 1918
};
export class Movie {
    constructor() {
        return movie;
    }
}