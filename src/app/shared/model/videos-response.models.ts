import { Video } from '@App/app/shared/model/video.model';

export interface VideosResponse {
    id: number;
    results: Video[];
}

export class VideosResponse implements VideosResponse {}