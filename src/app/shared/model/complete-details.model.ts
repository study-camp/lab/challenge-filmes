import { MovieDetails } from "./movie-details.model";
import { Movie } from "./movie.model";
import { People } from "./people.model";
import { ReleaseDateResult } from "./release-dates-response.model";
import { Video } from "./video.model";

export interface CompleteDetails {
    details: MovieDetails;
    releases: ReleaseDateResult[];
    cast: People[];
    crew: People[];
    recommendations: Movie[];
    videos: Video[];
}

export class CompleteDetails implements CompleteDetails {}