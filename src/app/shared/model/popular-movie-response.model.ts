import { Movie } from "./movie.model";

export interface PopularMovieResponse {
    page: number;
    results: Movie[];
    total_pages: number;
    total_results: number;
}

export class PopularMovieResponse implements PopularMovieResponse {}