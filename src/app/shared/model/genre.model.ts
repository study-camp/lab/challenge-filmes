export interface Genre {
    id: number;
    name: string;
}

export class Genre implements Genre { }