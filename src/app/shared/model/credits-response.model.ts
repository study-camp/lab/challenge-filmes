import { People } from "./people.model";

export interface CreditsResponse {
    id: number;
    cast: People[];
    crew: People[];
}

export class CreditsResponse implements CreditsResponse {}