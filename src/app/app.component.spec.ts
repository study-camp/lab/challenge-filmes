import { test, describe, expect, beforeEach } from '@jest/globals';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { HeaderModule } from './shared/component/header/header.module';
import { By } from '@angular/platform-browser';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HeaderModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
  });

  test('should create the app', async () => {
    expect(app).toBeTruthy();
  });

  test(`the title is 'challenge-filmes'`, async () => {
    expect(app.title).toBe('Homem de Ferro Films');
  });

  test(`the page has a app-header`, async () => {
    const appHeader = fixture.debugElement.query(By.css('app-header'));
    expect(appHeader).toBeTruthy();
  });

  test(`the page has a router-outlet`, async () => {
    const routerOutlet = fixture.debugElement.query(By.css('router-outlet'));
    expect(routerOutlet).toBeTruthy();
  });

  test(`the page has a footer`, async () => {
    const footer = fixture.nativeElement.querySelector('footer');
    expect(footer).toBeTruthy();
  });

});
