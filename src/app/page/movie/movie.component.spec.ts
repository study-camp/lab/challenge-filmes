import { test, describe, expect, beforeEach, jest } from '@jest/globals';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MovieComponent } from './movie.component';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { DetailsComponent } from './details/details.component';
import { RecommendationsComponent } from './recommendations/recommendations.component';
import { TrailerComponent } from './trailer/trailer.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCardModule } from '@angular/material/card';
import { CustomPipeModule } from '@App/app/shared/pipe/pipe.module';
import { MovieListModule } from '@App/app/shared/component/movie-list/movie-list.module';
import { CompleteDetails } from '@App/app/shared/model/complete-details.model';
import { CreditsResponse } from '@App/app/shared/model/credits-response.model';
import { MovieDetails } from '@App/app/shared/model/movie-details.model';
import { ReleaseDatesResponse } from '@App/app/shared/model/release-dates-response.model';
import { PopularMovieResponse } from '@App/app/shared/model/popular-movie-response.model';
import { VideosResponse } from '@App/app/shared/model/videos-response.models';
import { CastComponent } from './cast/cast.component';


jest
  .mock('@App/app/shared/model/movie-details.model')
  .mock('@App/app/shared/model/credits-response.model')
  .mock('@App/app/shared/model/release-dates-response.model')
  .mock('@App/app/shared/model/popular-movie-response.model')
  .mock('@App/app/shared/model/videos-response.models')
  .mock('@App/app/shared/model/complete-details.model');

const DEFAULT_CREW_SIZE = 4;
const DEFAULT_RECOMMENDATION_COLS = 6;
const DEFAULT_RECOMMENDATION_MAX_RESULTS = 4;


describe('MovieComponent', () => {
  let component: MovieComponent;
  let fixture: ComponentFixture<MovieComponent>;
  const mockMovieDetails = new CompleteDetails();
  const activatedRouteMock = {
    snapshot: {
      data: {
        movieDetails: mockMovieDetails,
      },
    },
  };
  beforeEach(async () => {
    jest.clearAllMocks();
    const credits = new CreditsResponse();
    mockMovieDetails.details = new MovieDetails();
    mockMovieDetails.releases = new ReleaseDatesResponse().results;
    mockMovieDetails.crew = credits.crew;
    mockMovieDetails.cast = credits.cast;
    mockMovieDetails.recommendations = new PopularMovieResponse().results;
    mockMovieDetails.videos = new VideosResponse().results;

    jest
      .spyOn(BreakpointObserver.prototype, 'observe')
      .mockImplementation(() => of({
        matches: true,
        breakpoints: {
          '(min-width: 1440px)': true
        }
      }));

    await TestBed.configureTestingModule({
      declarations: [ 
        MovieComponent, 
        DetailsComponent, 
        CastComponent,
        RecommendationsComponent, 
        TrailerComponent
      ],
      imports: [
        MatProgressSpinnerModule,
        MatCardModule,
        CustomPipeModule,
        MovieListModule,
      ],
      providers: [
        {
          provide: ActivatedRoute, 
          useValue: activatedRouteMock
        },
        BreakpointObserver
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MovieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('should have the size values to (min-width: 1440px) ', () => {
    expect(component.detailsMaxCrew).toBe(6);
    expect(component.recommendationCols).toBe(6);
    expect(component.recommendationMaxResults).toBe(6);
  });

  test('should have the size values to default ones', () => {
    component.updatePageSizeValues('FAIL')
    expect(component.detailsMaxCrew).toBe(DEFAULT_CREW_SIZE);
    expect(component.recommendationCols).toBe(DEFAULT_RECOMMENDATION_COLS);
    expect(component.recommendationMaxResults).toBe(DEFAULT_RECOMMENDATION_MAX_RESULTS);
  });
});
