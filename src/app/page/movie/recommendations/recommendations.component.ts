import { Component, Input } from '@angular/core';

import { Movie } from '@App/app/shared/model/movie.model';

@Component({
  selector: 'movie-recommendations',
  templateUrl: './recommendations.component.html',
  styleUrls: ['./recommendations.component.sass']
})
export class RecommendationsComponent {
  @Input()
  recommendations: Movie[] = [];

  @Input('cols')
  cols!: number;
  
  @Input()
  maxResults = 4;

  getRecommendations(): Movie[] {
    return this.recommendations.slice(0, this.maxResults);
  }
}
