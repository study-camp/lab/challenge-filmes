import { test, describe, expect, beforeEach, jest } from '@jest/globals';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MovieListModule } from '@App/app/shared/component/movie-list/movie-list.module';

import { RecommendationsComponent } from './recommendations.component';
import { Movie } from '@App/app/shared/model/movie.model';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';

jest
  .mock('@App/app/shared/model/movie.model');

const MOVIE_LIST_SIZE = 10;
const DEFAULT_MOVIE_LIST_SIZE = 4;
describe('RecommendationsComponent', () => {
  let component: RecommendationsComponent;
  let fixture: ComponentFixture<RecommendationsComponent>;
  const mockRecommendations = Array(MOVIE_LIST_SIZE).fill(new Movie());
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecommendationsComponent ],
      imports: [
        MovieListModule,
        RouterTestingModule,
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RecommendationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('should create a movie-list', async () => {
    component.recommendations = mockRecommendations;
    fixture.detectChanges();
    await fixture.whenStable();
    const movieList = fixture.debugElement.query(By.css('movie-list'));
    expect(movieList).toBeTruthy();
    expect(component.getRecommendations()).toHaveLength(DEFAULT_MOVIE_LIST_SIZE);
  });
  test('should create a movie-list with limited size', async () => {
    component.recommendations = mockRecommendations;
    component.maxResults = MOVIE_LIST_SIZE
    fixture.detectChanges();
    await fixture.whenStable();
    expect(component.getRecommendations()).toHaveLength(MOVIE_LIST_SIZE);
  });

});
