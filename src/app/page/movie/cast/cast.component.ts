import { Component, Input } from '@angular/core';

import { People } from '@App/app/shared/model/people.model';
import { environment } from '@App/environments/environment';

@Component({
  selector: 'movie-cast',
  templateUrl: './cast.component.html',
  styleUrls: ['./cast.component.sass']
})
export class CastComponent {
  @Input()
  cast: People[] = [];
  imgPath = environment.api.posterBaseUrl;
  avatarPlaceHolder = environment.api.castPhotoPlaceHolder;
  imgSize = 'w185/';

  getImagePath(people: People) {
    if(people.profile_path == null) {
      return this.avatarPlaceHolder;
    }
    return `${this.imgPath}${this.imgSize}${people.profile_path}`;
  }
}
