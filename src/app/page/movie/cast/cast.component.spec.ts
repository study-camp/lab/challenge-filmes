import { test, describe, expect, beforeEach, jest } from '@jest/globals';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';

import { CastComponent } from './cast.component';
import { People } from '@App/app/shared/model/people.model';
import { environment } from '@App/environments/environment';
import { MatCardModule } from '@angular/material/card';
import { ChangeDetectionStrategy } from '@angular/core';
import { MatCardHarness } from '@angular/material/card/testing';
import { By } from '@angular/platform-browser';

jest.mock('@App/app/shared/model/people.model');

const CAST_SIZE = 10;

describe('CastComponent', () => {
  let component: CastComponent;
  let fixture: ComponentFixture<CastComponent>;
  const imgPath = environment.api.posterBaseUrl;
  const avatarPlaceHolder = environment.api.castPhotoPlaceHolder;

  beforeEach(async () => {
    jest.clearAllMocks();
    await TestBed.configureTestingModule({
      declarations: [ CastComponent ],
      imports: [MatCardModule],
    })
    .overrideComponent(CastComponent, {
      set: { changeDetection: ChangeDetectionStrategy.Default }
    })
    .compileComponents();

    fixture = TestBed.createComponent(CastComponent);
    component = fixture.componentInstance;
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('getImagePath should return the correct url', () => {
    const people = new People();
    const expectedUrl = `${imgPath}${component.imgSize}${people.profile_path}`;
    const result = component.getImagePath(people);
    expect(result).toBe(expectedUrl);
  });

  test('getImagePath should return the placeholder', () => {
    const people = new People();
    people.profile_path = null;
    const result = component.getImagePath(people);
    expect(result).toBe(avatarPlaceHolder);
  });

  it('set the cast should create a list of MatCards', async () => {
    const people = new People()
    const expectedImageUrl = component.getImagePath(people);
    component.cast = Array(CAST_SIZE).fill(people);

    await fixture.whenStable()
    const loader = TestbedHarnessEnvironment.loader(fixture);
    const cards = await loader.getAllHarnesses(MatCardHarness);
    
    const card = cards[0];
    const title = await card.getTitleText();
    const subtitle = await card.getSubtitleText();
    const image = fixture.debugElement.query(By.css('img')).nativeElement;

    expect(cards).toHaveLength(CAST_SIZE);
    expect(image.getAttribute('src')).toBe(expectedImageUrl);
    expect(title).toBe(people.name);
    expect(subtitle).toBe(people.character);
  });

});
