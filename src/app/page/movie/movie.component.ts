import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';

import { CompleteDetails } from '@App/app/shared/model/complete-details.model';

const DEFAULT_CREW_SIZE = 4;
const DEFAULT_RECOMMENDATION_COLS = 6;
const DEFAULT_RECOMMENDATION_MAX_RESULTS = 4;

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.sass']
})
export class MovieComponent implements OnInit, OnDestroy {
  destroyed = new Subject<void>();
  sizes = new Map([
    ['(max-width: 736px)', { maxCrew: 4, recommendationCols: 2, recommendationMaxResults: 6 }],
    ['(min-width: 736px) and (max-width: 819px)', { maxCrew: 4, recommendationCols: 3, recommendationMaxResults: 6 }],
    ['(min-width: 820px) and (max-width: 895px)', { maxCrew: 4, recommendationCols: 4, recommendationMaxResults: 6 }],
    ['(min-width: 896px) and (max-width: 1279px)', { maxCrew: 6, recommendationCols: 5, recommendationMaxResults: 5 }],
    ['(min-width: 1280px) and (max-width: 1439px)', { maxCrew: 6, recommendationCols: 6, recommendationMaxResults: 6 }],
    ['(min-width: 1440px)', { maxCrew: 6, recommendationCols: 6, recommendationMaxResults: 6 }],
  ]);

  movieDetails!: CompleteDetails;
  detailsMaxCrew = DEFAULT_CREW_SIZE;
  recommendationCols = DEFAULT_RECOMMENDATION_COLS;
  recommendationMaxResults = DEFAULT_RECOMMENDATION_MAX_RESULTS;

  constructor(
    private route: ActivatedRoute,
    private breakpointObserver: BreakpointObserver,
    ) {
      this.breakpointObserver
        .observe([...this.sizes.keys()])
        .pipe(takeUntil(this.destroyed))
        .subscribe(result => {
          for (const query of Object.keys(result.breakpoints)) {
            if (result.breakpoints[query]) {
              this.updatePageSizeValues(query);
            }
          }
        });
    }

  ngOnInit(): void {
    this.movieDetails = this.route.snapshot.data['movieDetails'];
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  updatePageSizeValues(query: string) {
    this.detailsMaxCrew = this.sizes.get(query)?.maxCrew
      || DEFAULT_CREW_SIZE;
    this.recommendationCols = this.sizes.get(query)?.recommendationCols
      || DEFAULT_RECOMMENDATION_COLS;
    this.recommendationMaxResults = this.sizes.get(query)?.recommendationMaxResults
      || DEFAULT_RECOMMENDATION_MAX_RESULTS;
  }
}
