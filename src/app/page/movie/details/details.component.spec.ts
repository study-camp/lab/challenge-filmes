import { expect, describe, beforeEach, test, jest } from '@jest/globals';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RouterTestingModule } from '@angular/router/testing';
import { CompleteDetails } from '@App/app/shared/model/complete-details.model';
import { CreditsResponse } from '@App/app/shared/model/credits-response.model';
import { MovieDetails } from '@App/app/shared/model/movie-details.model';
import { ReleaseDatesResponse } from '@App/app/shared/model/release-dates-response.model';
import { CustomPipeModule } from '@App/app/shared/pipe/pipe.module';
import { MovieComponent } from '../movie.component';

import { DetailsComponent } from './details.component';

jest
  .mock('@App/app/shared/model/movie-details.model')
  .mock('@App/app/shared/model/credits-response.model')
  .mock('@App/app/shared/model/release-dates-response.model')
  .mock('@App/app/shared/model/complete-details.model');

const ISO_3166_1 = 'BR';
describe('DetailsComponent', () => {
  let component: DetailsComponent;
  let fixture: ComponentFixture<DetailsComponent>;
  
  const completeDetails = new CompleteDetails();
  let movideDetails: MovieDetails;
  
  beforeEach(async () => {
    jest.clearAllMocks();
    const credits = new CreditsResponse();
    completeDetails.details = new MovieDetails();
    completeDetails.releases = new ReleaseDatesResponse().results;
    completeDetails.crew = credits.crew;

    await TestBed.configureTestingModule({
      declarations: [ DetailsComponent ],
      imports: [
        RouterTestingModule.withRoutes([
          { path: '', component: MovieComponent },
        ]), 
        CustomPipeModule,
        MatProgressSpinnerModule
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DetailsComponent);
    component = fixture.componentInstance;
    component.movieDetails = completeDetails;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('call getReleasesFrom should return date', () => {
    const expectedDate = completeDetails.releases
      .find(item => item.iso_3166_1 == ISO_3166_1)?.release_dates[0];
    const release = component.getReleasesFrom(ISO_3166_1);
    expect(release).not.toBeNull();
    expect(release?.release_date).toBe(expectedDate?.release_date);
  });

  test('call getReleasesFrom should return null', () => {
    const release = component.getReleasesFrom(ISO_3166_1 + 'FAIL');
    expect(release).toBeNull();
  });

  test('call getReleaseDate should Brasil releaseDate', () => {
    const expectedDate = completeDetails.releases
      .find(item => item.iso_3166_1 == ISO_3166_1)?.release_dates[0];

    const release = component.getReleaseDate();
    expect(release).toBe(expectedDate!);
  });

  test('call getReleaseDate should US releaseDate', () => {
    completeDetails.releases = completeDetails.releases.filter(item => item.iso_3166_1 != ISO_3166_1);
    component.movieDetails = completeDetails;
    const expectedDate = completeDetails.releases
      .find(item => item.iso_3166_1 == 'US')?.release_dates[0];

    const release = component.getReleaseDate();
    expect(release).toBe(expectedDate!);
  });

  test('call getReleaseDate should US releaseDate', () => {
    completeDetails.releases = completeDetails.releases.filter(item => item.iso_3166_1 != ISO_3166_1 && item.iso_3166_1 != 'US');

    component.movieDetails = completeDetails;

    const release = component.getReleaseDate();
    expect(release).toBeNull();
  });

  test('should show the release date', async () => {
    const expectedDate = completeDetails.releases
      .find(item => item.iso_3166_1 == ISO_3166_1)?.release_dates[0];
    expect(component.release).toBe(expectedDate!);
  });

  test('should show the release_date from details not from releases', async () => {
    component.movieDetails.releases = [];
    component.ngOnInit();
    fixture.detectChanges();
    await fixture.whenStable();
    expect(component.releaseDate).toBe(completeDetails.details.release_date);
  });
  test('call getPosterUrl should return placeholder', async () => {
    completeDetails.details.poster_path = '';
    const resultUrl = component.getPosterUrl();
    expect(resultUrl).toBe(component.posterPlaceHolder);
  });

  test('call getAverage should return ZERO', async () => {
    completeDetails.details.vote_average = 0;
    const result = component.getAverage();
    expect(result).toBe(0);
  });

});
