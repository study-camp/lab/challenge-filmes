import { Component, Input, OnInit} from '@angular/core';
import { CompleteDetails } from '@App/app/shared/model/complete-details.model';
import { ReleaseDate } from '@App/app/shared/model/release-dates-response.model';
import { People } from '@App/app/shared/model/people.model';
import { environment } from '@App/environments/environment';
import { MovieDetails } from '@App/app/shared/model/movie-details.model';

@Component({
  selector: 'movie-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.sass']
})
export class DetailsComponent implements OnInit {

  
  @Input() movieDetails!: CompleteDetails;
  @Input() maxCrew = 4
  imagePath = environment.api.posterBaseUrl;
  posterPlaceHolder = environment.api.posterPlaceHolder;
  posterSize = 'w500';

  release: ReleaseDate | null = null;
  releaseDate: Date | null = null;

  ngOnInit() {
    this.updatePage();
  }

  updatePage() {
      this.release = this.getReleaseDate();
      this.releaseDate = this.release?.release_date 
        ?? this.movieDetails.details.release_date;
  }

  getReleaseDate() {
    const releaseDate = this.getReleasesFrom('BR') ?? this.getReleasesFrom('US');
    return releaseDate || null;
  }

  getReleasesFrom(iso_3166_1: string): ReleaseDate | null {
    return this.movieDetails.releases
      .filter(result => result.iso_3166_1 == iso_3166_1)
      .find(result => result.release_dates.some(release => release.certification != ''))
      ?.release_dates[0] || null;
  }

  getAverage(): number {
    if (!this.movieDetails.details.vote_average || this.movieDetails.details.vote_average === 0) {
      return 0;
    }
    return Math.round(this.movieDetails.details.vote_average * 10);
  }

  getCrew(): People[] {
    return this.movieDetails.crew.slice(0, this.maxCrew);
  }

  getGenres(): string {
    return this.movieDetails.details.genres.map(genre => genre.name).join(', ');
  }

  getPosterUrl() {
    if (this.movieDetails.details.poster_path == null || this.movieDetails.details.poster_path == '') {
      return this.posterPlaceHolder;
    }
    return this.imagePath + this.posterSize + '/' + this.movieDetails.details.poster_path;
  }
}
