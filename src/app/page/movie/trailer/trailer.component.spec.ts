import { test, describe, expect, beforeEach, jest } from '@jest/globals';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TrailerComponent } from './trailer.component';
import { VideosResponse } from '@App/app/shared/model/videos-response.models';

jest.mock('@App/app/shared/model/videos-response.models');

const traillers = new VideosResponse();

describe('TrailerComponent', () => {
  let component: TrailerComponent;
  let fixture: ComponentFixture<TrailerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrailerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TrailerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('should return url from video', () => {
    const url = component.sanitizeUrl(traillers.results[0]);
    expect(url).not.toBeNull();
  });


  test('should return null from video', () => {
    jest.spyOn(window.console, 'error').mockImplementationOnce(_ => {});
    const video = traillers.results[0];
    video.site = 'yvideos';
    const url = component.sanitizeUrl(video);
    expect(url).toBeNull();
  });

  test('should create a list of videos', () => {
    component.videos = traillers.results;
    expect(component).toBeTruthy();
  });
});
