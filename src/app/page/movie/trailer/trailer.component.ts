import { Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Video } from '@App/app/shared/model/video.model';

@Component({
  selector: 'movie-trailer',
  templateUrl: './trailer.component.html',
  styleUrls: ['./trailer.component.sass']
})
export class TrailerComponent {
  @Input()
  videos!: Video[];

  videoProviders = new Map([
    ['youtube', 'https://www.youtube.com/embed/'],
    ['vimeo', 'https://player.vimeo.com/video/']
  ]);

  constructor(private sanitizer: DomSanitizer) {}

  sanitizeUrl(video: Video) {
    const site = video.site.toLowerCase();
    if (!this.videoProviders.get(site)) {
      console.error(`Provider "${site}" not defined.`);
      return null;
    }
    const url = this.videoProviders.get(site) + video.key;
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
