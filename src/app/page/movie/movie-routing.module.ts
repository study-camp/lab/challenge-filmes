import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MovieDetailsResolver } from '@App/app/core/guard/movie-details.resolver';
import { MovieComponent } from './movie.component';

const routes: Routes = [
  { 
    path: ':id', 
    component: MovieComponent, 
    resolve: { movieDetails: MovieDetailsResolver } 
  },
  { path: '', component: MovieComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovieRoutingModule { }
