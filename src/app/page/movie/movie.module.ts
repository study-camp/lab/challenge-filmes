import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule} from '@angular/material/progress-spinner'

import { CustomPipeModule } from '@App/app/shared/pipe/pipe.module';
import { MovieListModule } from '@App/app/shared/component/movie-list/movie-list.module';
import { DetailsComponent } from './details/details.component';
import { CastComponent } from './cast/cast.component';
import { TrailerComponent } from './trailer/trailer.component';
import { RecommendationsComponent } from './recommendations/recommendations.component';
import { MovieRoutingModule } from './movie-routing.module';
import { MovieComponent } from './movie.component';

@NgModule({
  declarations: [
    MovieComponent,
    DetailsComponent,
    CastComponent,
    TrailerComponent,
    RecommendationsComponent
  ],
  imports: [
    CommonModule,
    MovieRoutingModule,
    MatProgressSpinnerModule,
    MatCardModule,
    CustomPipeModule,
    MovieListModule,
  ]
})
export class MovieModule { }
