import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '@angular/cdk/layout';
import { RouterModule } from '@angular/router';

import { MovieListModule } from '@App/app/shared/component/movie-list/movie-list.module';
import { PaginatorModule } from '@App/app/shared/component/paginator/paginator.module';
import { HomeRoutingModule } from './home-routing.module';
import { TopNavModule } from "./top-nav/top-nav.module";
import { HomeComponent } from './home.component';
@NgModule({
    declarations: [
        HomeComponent
    ],
    exports: [
        HomeComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        LayoutModule,
        HomeRoutingModule,
        TopNavModule,
        MovieListModule,
        PaginatorModule
    ]
})
export class HomeModule { }
