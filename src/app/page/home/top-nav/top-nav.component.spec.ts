import { expect, describe, beforeEach, test } from '@jest/globals';

import { ComponentFixture, ComponentFixtureAutoDetect, TestBed } from '@angular/core/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { 
  MatButtonToggleHarness, 
  MatButtonToggleGroupHarness 
} from '@angular/material/button-toggle/testing';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { Genre } from '../../../shared/model/genre.model';

import { TopNavComponent } from './top-nav.component';
import { ChangeDetectionStrategy } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
const TWO = 2;
const ONE = 1;
const ZERO = 0;
describe('TopNavComponent', () => {
  let component: TopNavComponent;
  let fixture: ComponentFixture<TopNavComponent>;

  beforeEach( async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopNavComponent ],
      imports: [ 
        MatButtonToggleModule,
        FormsModule,
        MatIconModule
      ],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true }
      ]
    })
      .overrideComponent(TopNavComponent, {
      set: { changeDetection: ChangeDetectionStrategy.Default }
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TopNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create any buttons', async () => {
    component.buttons = [];
    const loader = TestbedHarnessEnvironment.loader(fixture);
    const buttonsElements = await loader.getAllHarnesses(MatButtonToggleHarness);
    expect(buttonsElements.length).toBe(ZERO);
  });

  it('should create buttons', async () => {
    const genres: Genre[] = [
      { id: ONE, name: 'Aventura' }, 
      { id: TWO, name: 'Teste' }
    ];
    component.buttons = genres;
    const loader = TestbedHarnessEnvironment.loader(fixture);
    const buttonsElements = await loader.getAllHarnesses(MatButtonToggleHarness);
    expect(buttonsElements.length).toBe(TWO); 
  });

  it('should create a button and click it', async () => {
    const componentSpy = jest.spyOn(component, 'triggerEvent');
    const genres: Genre[] = [{ id: 1, name: 'Aventura' }];
    component.buttons = genres;
    const loader = TestbedHarnessEnvironment.loader(fixture);
    const button = await loader.getHarness(MatButtonToggleHarness);
    const host = await button.host();
    await host.click();
    expect(componentSpy).toBeCalledTimes(ONE);
  });

  it('should create a button and check it', async () => {
    const genres: Genre[] = [{ id: 1, name: 'Aventura' }];
    component.buttons = genres;
    const loader = TestbedHarnessEnvironment.loader(fixture);
    const group = await loader.getHarness(MatButtonToggleGroupHarness);
    await group.getToggles()
      .then(async (buttons) => await buttons[0].check());
    expect(component.appliedFilters[0]).toBe(ONE);
  });

});
