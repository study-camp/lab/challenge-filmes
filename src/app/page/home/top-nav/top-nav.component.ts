import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Genre } from '@App/app/shared/model/genre.model';

@Component({
  selector: 'top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.sass']
})
export class TopNavComponent {
  @Input()
  buttons: Genre[] = [];
  @Input('filters')
  appliedFilters: number[] = [];

  @Output()
  filterSelected = new EventEmitter();


  triggerEvent() {
    this.filterSelected.emit({ filters: this.appliedFilters });
  }

}
