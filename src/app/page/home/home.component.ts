import { Component, OnDestroy, OnInit } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { ActivatedRoute, Params } from '@angular/router';
import { EMPTY, Observable, Subject, takeUntil } from 'rxjs';

import { GenresService } from '@App/app/core/service/genres.service';
import { MoviesService } from '@App/app/core/service/movies.service';
import { PopularMovieResponse } from '@App/app/shared/model/popular-movie-response.model';
import { SessionService } from '@App/app/core/service/session.service';
import { Movie } from '@App/app/shared/model/movie.model';
import { Genre } from '@App/app/shared/model/genre.model';

const DEFAULT_LIST_COLS = 2;
const DEFAULT_PAGINATOR_SIZE = 3;
const FILTERS = 'filters';
const FIRST = 1;
@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit, OnDestroy {
  destroyed = new Subject<void>();
  sizes = new Map([
    ['(max-width: 416px)', { cols: 2}],
    ['(min-width: 417px) and (max-width: 576px)', { cols: 2, paginatorSize: 3 }],
    ['(min-width: 577px) and (max-width: 704px)', { cols: 3, paginatorSize: 4 }],
    ['(min-width: 705px) and (max-width: 864px)', { cols: 4, paginatorSize: 5 }],
    ['(min-width: 865px)', { cols: 5, paginatorSize: 5 }],
  ]);
  genres$: Observable<Genre[]> = EMPTY;
  popularMovies!: PopularMovieResponse;
  appliedFilters: number[] = [];
  movieList: Movie[] = [];
  activePage = FIRST;
  movieListCols = DEFAULT_LIST_COLS;
  paginatorSize = DEFAULT_PAGINATOR_SIZE;
  
  constructor(
    private breakpointObserver: BreakpointObserver,
    private genresService: GenresService,
    private moviesService: MoviesService,
    private route: ActivatedRoute,
    private session: SessionService,
  ) {
    this.breakpointObserver
      .observe([...this.sizes.keys()])
      .pipe(takeUntil(this.destroyed))
      .subscribe(result => {
          for (const query of Object.keys(result.breakpoints)) {
            if (result.breakpoints[query]) {
              this.updateComponentSize(query);
            }
          }
      });
  }

  ngOnInit(): void {
    this.appliedFilters = this.session.getArrayData(FILTERS);
    this.route.queryParamMap
      .subscribe({
        next: (params: Params) => {
          this.activePage = params['params'].page || FIRST;
          this.moviesService.getMoviePopularList(this.activePage).subscribe({
            next: result => { 
              this.popularMovies = result;
              this.updateMovieList();
            },
            error: error => console.error(error)
          });
        }
      });
    this.genres$ = this.genresService.getGenreList();
  }

  updateComponentSize(query: string) {
    this.movieListCols = this.sizes.get(query)?.cols ?? DEFAULT_LIST_COLS;
    this.paginatorSize = this.sizes.get(query)?.paginatorSize ?? DEFAULT_PAGINATOR_SIZE;
  }

  updateMovieList() {
    if (this.appliedFilters.length == 0) {
      this.movieList = this.popularMovies.results;
      return;
    }
    this.movieList = this.popularMovies.results.filter(
      movie => movie.genre_ids.some(id => this.appliedFilters.includes(id))
    );
  }

  changeFilter(event: any) {
    this.appliedFilters = [...event.filters];
    this.updateMovieList();
  }

  ngOnDestroy() {
    this.session.setArrayData(FILTERS,this.appliedFilters);
    this.destroyed.next();
    this.destroyed.complete();
  }
}
