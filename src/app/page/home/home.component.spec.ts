import { expect, describe, beforeEach, test, jest } from '@jest/globals';

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EMPTY, of, throwError } from 'rxjs';
import { MoviesService } from '@App/app/core/service/movies.service';
import { GenresService } from '@App/app/core/service/genres.service';
import { HomeComponent } from './home.component';
import { TopNavModule } from './top-nav/top-nav.module';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { BreakpointObserver } from '@angular/cdk/layout';
import { SessionService } from '@App/app/core/service/session.service';
import { ChangeDetectionStrategy } from '@angular/core';
import { PopularMovieResponse } from '@App/app/shared/model/popular-movie-response.model';

jest
  .mock('@App/app/shared/model/popular-movie-response.model')
  .mock('@App/app/core/service/session.service')
  .mock('@App/app/core/service/genres.service')
  .mock('@App/app/core/service/movies.service')
  .mock('@App/app/shared/model/movie.model')
;

const ZERO = 0;
const ONE = 1;
const TWO = 2;
const THREE = 3;
const FIVE = 5;
const MOVIE_ERROR = 'Movie Error';
const LIST_SIZE = 20;
const EMPTY_ARRAY:number[] = [];
describe('HomeComponent', () => {
  jest
    .spyOn(BreakpointObserver.prototype, 'observe')
    .mockImplementation(() => of({
      matches: true,
      breakpoints: {
        '(min-width: 865px)': true
      }
    }));

  const activatedRouteSpy = jest
    .spyOn(ActivatedRoute.prototype, 'queryParamMap', 'get')
    .mockImplementation(() => of(convertToParamMap({ page: TWO })));
  
  const spySessionService = jest
    .spyOn(SessionService.prototype, 'getArrayData')
    .mockReturnValue(EMPTY_ARRAY);

  const spyMoviServicePopular = jest
    .spyOn(MoviesService.prototype, 'getMoviePopularList')
    .mockImplementation(() => EMPTY);

  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  
  beforeEach(async () => {
    jest.clearAllMocks();
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports: [
        TopNavModule,
        RouterTestingModule.withRoutes([
          { path: '', component: HomeComponent },
        ])
      ],
      providers: [
        BreakpointObserver,
        GenresService,
        MoviesService,
        SessionService,
        { provide: ActivatedRoute, useValue: new ActivatedRoute() },
      ]
    })
    .overrideComponent(HomeComponent, {
      set: { changeDetection: ChangeDetectionStrategy.Default }
    })
    .compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.autoDetectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('should test screen size change', () => {
    expect(component.movieListCols).toBe(FIVE);
    expect(component.paginatorSize).toBe(FIVE);
  });

  test('should test screen size with default values', () => {
    component.updateComponentSize('');
    expect(component.movieListCols).toBe(TWO);
    expect(component.paginatorSize).toBe(THREE);
  });

  it('ngOnInit should call the ActiveRoute but fail on get a movie list', async () => {
    activatedRouteSpy.mockImplementationOnce(
      () => of(convertToParamMap({}))
    );
    const expectedError = new Error(MOVIE_ERROR);
    spyMoviServicePopular.mockReturnValueOnce(throwError(() => expectedError));
    const spyConsoleError = jest.spyOn(window.console, 'error').mockImplementation(_ => {});
    component.ngOnInit();
    await fixture.whenStable();
    expect(activatedRouteSpy).toHaveBeenCalled();
    expect(component.activePage).toBe(ONE);
    expect(spyConsoleError).toBeCalledWith(expectedError);
  });

  it('executing ngOnInit should call getMoviePopularList and receive list', async () => {
    const movies = new PopularMovieResponse();
    spyMoviServicePopular.mockReturnValueOnce(of(movies));
    component.ngOnInit();
    await fixture.whenStable();
    expect(spySessionService).toHaveBeenCalled();
    expect(component.appliedFilters).toBe(EMPTY_ARRAY);
    expect(component.activePage).toBe(TWO);
    expect(component.popularMovies).toBe(movies);
  });

  it('executing ngOnInit should call getMoviePopularList and receive list', async () => {
    const filters = [1];
    const movies = new PopularMovieResponse();
    movies.results.forEach(movie => movie.genre_ids = [TWO]);
    movies.results[ZERO].genre_ids = [ONE];
    spyMoviServicePopular.mockReturnValueOnce(of(movies));
    component.ngOnInit();
    await fixture.whenStable();

    expect(component.appliedFilters).toBe(EMPTY_ARRAY);
    expect(component.movieList).toHaveLength(LIST_SIZE);

    component.changeFilter({ filters });
    await fixture.whenStable();

    expect(component.appliedFilters.toString()).toBe(filters.toString());
    expect(component.movieList).toHaveLength(ONE);
  });
});
