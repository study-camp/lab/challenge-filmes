import { test, describe, expect, beforeEach, jest } from '@jest/globals';
import { SpyInstance } from 'node_modules/jest-mock/build/index';
import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';

import { GenresService } from '@App/app/core/service/genres.service';
import { EMPTY, of } from 'rxjs';
import { environment } from '@App/environments/environment';
import { Genre } from '@App/app/shared/model/genre.model';

jest
    .mock('@angular/common/http')
    .mock('@App/app/shared/model/genre.model');

describe('SessionService', () => {
    let service: GenresService;
    let spyHttp: SpyInstance<any>;
    const api = environment.api;
    const language = 'pt-BR';
    const expectedUrl = `${api.url}genre/movie/list?api_key=${api.key}&language=${language}`;
    beforeEach(() => {
        jest.clearAllMocks();
        spyHttp = jest.spyOn(HttpClient.prototype, 'get')
            .mockImplementation(() => EMPTY);
        const http = new HttpClient(jest.mocked({} as HttpHandler, true));
        
        TestBed.configureTestingModule({
            imports: [ HttpClientModule ],
            providers: [
                { provide: HttpClient, useValue: http }
            ]
        });
        service = TestBed.inject(GenresService);
    });

    test('should be created', () => {
        expect(service).toBeTruthy();
    });

    test('should call getGenreList and receive a empty list', () => {
        service.getGenreList();
        expect(spyHttp).toBeCalledWith(expectedUrl);
    });

    test('should call getGenreList and receive a list with 2 values', done => {
        const genres: Genre[] = Array(2).fill(new Genre());
        spyHttp.mockReturnValueOnce(of({ genres }));
        let result:Genre[];
        expect.assertions(3);
        service.getGenreList().subscribe({
            next: (values: Genre[]) => { 
                result = values;
                expect(result).toHaveLength(2);
                expect(result).toBe(genres);
                expect(spyHttp).toBeCalledWith(expectedUrl);
                done();
            },
            error: error => {
                fail(error);
            }
        });
    });
});