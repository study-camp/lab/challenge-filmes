import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { first, map, Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Genre } from '../../shared/model/genre.model';

@Injectable({
    providedIn: 'root'
})
export class GenresService {
    private readonly api = environment.api;
    private readonly language = 'pt-BR';

    constructor(private http: HttpClient) {
    }

    getGenreList(): Observable<Genre[]> {
        const url = `${this.api.url}genre/movie/list?api_key=${this.api.key}&language=${this.language}`;
        return this.http.get<any>(url)
            .pipe(
                first(),
                map<any, Genre[]>((result:{ genres: Genre[]}) => result.genres)
            );
    }
}
