import { test, describe, expect, beforeEach, jest } from '@jest/globals';
import { SpyInstance } from 'node_modules/jest-mock/build/index';
import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';

import { MoviesService } from '@App/app/core/service/movies.service';
import { environment } from '@App/environments/environment';
import { EMPTY, of } from 'rxjs';
import { PopularMovieResponse } from '@App/app/shared/model/popular-movie-response.model';
import { MovieDetails } from '@App/app/shared/model/movie-details.model';
import { ReleaseDatesResponse } from '@App/app/shared/model/release-dates-response.model';
import { CreditsResponse } from '@App/app/shared/model/credits-response.model';
import { VideosResponse } from '@App/app/shared/model/videos-response.models';
jest
    .mock('@angular/common/http')
    .mock('@App/app/shared/model/popular-movie-response.model')
    .mock('@App/app/shared/model/movie-details.model')
    .mock('@App/app/shared/model/release-dates-response.model')
    .mock('@App/app/shared/model/credits-response.model')
    .mock('@App/app/shared/model/videos-response.models')
    ;
describe('SessionService', () => {
    let service: MoviesService;
    let spyHttp: SpyInstance<any>;
    const api = environment.api;
    const language = 'pt-BR';

    beforeEach(() => {
        jest.clearAllMocks();
        spyHttp = jest.spyOn(HttpClient.prototype, 'get')
            .mockImplementation(() => EMPTY);
        const http = new HttpClient(jest.mocked({} as HttpHandler, true));
        
        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [
                { provide: HttpClient, useValue: http }
            ]
        });
        service = TestBed.inject(MoviesService);
    });

    test('should be created', () => {
        expect(service).toBeTruthy();
    });

    test('should call getMoviePopularList and receive a list', done => {
        const page = 1;
        const response = new PopularMovieResponse();
        const expectedUrl = `${api.url}/movie/popular?api_key=${api.key}&language=${language}&page=${page}`;
        
        spyHttp.mockReturnValueOnce(of(response));

        let result: PopularMovieResponse;
        expect.assertions(2);
        service.getMoviePopularList().subscribe({
            next: (values: PopularMovieResponse) => {
                result = values;
                expect(spyHttp).toBeCalledWith(expectedUrl);
                expect(result).toBe(response);
                done();
            },
            error: error => {
                fail(error);
            }
        });
    });

    test('should call getMovieDetails and receive a list', done => {
        const id = 1;
        const response = new MovieDetails();
        const expectedUrl = `${api.url}/movie/${id}?api_key=${api.key}&language=${language}`;

        spyHttp.mockReturnValueOnce(of(response));

        let result: MovieDetails;
        expect.assertions(2);
        service.getMovieDetails(id).subscribe({
            next: (values: MovieDetails) => {
                result = values;
                expect(spyHttp).toBeCalledWith(expectedUrl);
                expect(result).toBe(response);
                done();
            },
            error: error => {
                fail(error);
            }
        });
    });

    test('should call getMovieRecommendations and receive a list', done => {
        const id = 1;
        const response = new PopularMovieResponse();
        const expectedUrl = `${api.url}/movie/${id}/recommendations?api_key=${api.key}&language=${language}&page=1`;
        spyHttp.mockReturnValueOnce(of(response));

        let result: PopularMovieResponse;
        expect.assertions(2);
        service.getMovieRecommendations(id).subscribe({
            next: (values: PopularMovieResponse) => {
                result = values;
                expect(spyHttp).toBeCalledWith(expectedUrl);
                expect(result).toBe(response);
                done();
            },
            error: error => {
                fail(error);
            }
        });
    });

    test('should call getMovieReleaseDates and receive a list', done => {
        const id = 1;
        const response = new ReleaseDatesResponse();
        const expectedUrl = `${api.url}/movie/${id}/release_dates?api_key=${api.key}`;

        spyHttp.mockReturnValueOnce(of(response));

        expect.assertions(2);
        service.getMovieReleaseDates(id).subscribe({
            next: (values: ReleaseDatesResponse) => {
                expect(spyHttp).toBeCalledWith(expectedUrl);
                expect(values).toBe(response);
                done()
            },
            error: error => {
                fail(error);
            }
        });
    });

    test('should call getMovieCredits and receive a list', done => {
        const id = 1;
        const response = new CreditsResponse();
        const expectedUrl = `${api.url}/movie/${id}/credits?api_key=${api.key}&language=${language}`;

        spyHttp.mockReturnValueOnce(of(response));

        expect.assertions(2);
        service.getMovieCredits(id).subscribe({
            next: (values: CreditsResponse) => {
                expect(spyHttp).toBeCalledWith(expectedUrl);
                expect(values).toBe(response);
                done()
            },
            error: error => {
                fail(error);
            }
        });
    });

    test('should call getMovieVideos and receive a list', done => {
        const id = 1;
        const response = new VideosResponse();
        const expectedUrl = `${api.url}/movie/${id}/videos?api_key=${api.key}&language=${language}`;

        spyHttp.mockReturnValueOnce(of(response));

        expect.assertions(2);
        service.getMovieVideos(id).subscribe({
            next: (values: VideosResponse) => {
                expect(spyHttp).toBeCalledWith(expectedUrl);
                expect(values).toBe(response);
                done();
            },
            error: error => {
                fail(error);
            }
        });
    });

});