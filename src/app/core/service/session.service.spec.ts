import { test, describe, expect, beforeEach, jest } from '@jest/globals';
import { SpyInstance } from 'node_modules/jest-mock/build/index';
import { TestBed } from '@angular/core/testing';

import { SessionService } from './session.service';

const EMPTY_ARRAY: string[] = [];
const TEST_ARRAY = [1,2,3];
const KEY = 'chave';
let spySessionGetData: SpyInstance<any>;
let spySessionSetData: SpyInstance<any>;
describe('SessionService', () => {
  let service: SessionService;

  beforeEach(() => {
    spySessionGetData = jest.spyOn(Storage.prototype, 'getItem');
    spySessionSetData = jest.spyOn(Storage.prototype, 'setItem');
    TestBed.configureTestingModule({});
    service = TestBed.inject(SessionService);
  });

  test('should be created', () => {
    expect(service).toBeTruthy();
  });

  test('should call getArrayData and get a empty list', () => {
    spySessionGetData.mockReturnValue(null);
    const received = service.getArrayData(KEY);
    expect(received).toHaveLength(0);
    expect(received).toEqual(EMPTY_ARRAY);
  });

  test('should call getArrayData and get a populated list', () => {
    spySessionGetData.mockReturnValue(JSON.stringify(TEST_ARRAY));
    const received = service.getArrayData(KEY);
    expect(received).toHaveLength(TEST_ARRAY.length);
    expect(received).toEqual(TEST_ARRAY);
  });

  test('should call setData with the especific value', () => {
    service.setArrayData(KEY, TEST_ARRAY);
    const textTestArray = JSON.stringify(TEST_ARRAY);
    expect(spySessionSetData).toBeCalledWith(KEY, textTestArray);
  });
});
