import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  public getArrayData(key: string): any[] {
    const data = window.sessionStorage.getItem(key);
    return data ? JSON.parse(data): [];
  }
  
  public setArrayData(key: string, data: any[]) {
    window.sessionStorage.setItem(key, JSON.stringify(data));
  }
}
