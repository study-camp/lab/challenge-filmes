import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { first, Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { PopularMovieResponse } from '../../shared/model/popular-movie-response.model';
import { ReleaseDatesResponse } from '../../shared/model/release-dates-response.model';
import { MovieDetails } from '../../shared/model/movie-details.model';
import { CreditsResponse } from '../../shared/model/credits-response.model';
import { VideosResponse } from '../../shared/model/videos-response.models';

@Injectable({
    providedIn: 'root'
})
export class MoviesService {
    private readonly api = environment.api;
    private readonly language = 'pt-BR';

    constructor(private http: HttpClient) {
    }

    getMoviePopularList(page: number = 1): Observable<PopularMovieResponse> {
        const url = `${this.api.url}/movie/popular?api_key=${this.api.key}&language=${this.language}&page=${page}`;
        return this.http.get<PopularMovieResponse>(url)
            .pipe(first());
    }

    getMovieDetails(id: number): Observable<MovieDetails> {
        const url = `${this.api.url}/movie/${id}?api_key=${this.api.key}&language=${this.language}`;
        return this.http.get<MovieDetails>(url)
            .pipe(first());
    }

    getMovieRecommendations(id: number, page: number = 1): Observable<PopularMovieResponse> {
        const url = `${this.api.url}/movie/${id}/recommendations?api_key=${this.api.key}&language=${this.language}&page=1`;
        return this.http.get<PopularMovieResponse>(url)
            .pipe(first());
    }

    getMovieReleaseDates(id: number): Observable<ReleaseDatesResponse> {
        const url = `${this.api.url}/movie/${id}/release_dates?api_key=${this.api.key}`;
        return this.http.get<ReleaseDatesResponse>(url)
            .pipe(first());
    }

    getMovieCredits(id: number): Observable<CreditsResponse> {
        const url = `${this.api.url}/movie/${id}/credits?api_key=${this.api.key}&language=${this.language}`;
        return this.http.get<CreditsResponse>(url)
            .pipe(first());
    }

    getMovieVideos(id: number): Observable<VideosResponse> {
        const url = `${this.api.url}/movie/${id}/videos?api_key=${this.api.key}&language=${this.language}`;
        return this.http.get<VideosResponse>(url)
            .pipe(first());
    }
}
