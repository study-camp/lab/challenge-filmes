import { expect, describe, beforeEach, test, jest,  } from '@jest/globals';
import { SpyInstance } from 'node_modules/jest-mock/build';
import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { EMPTY, of, throwError } from 'rxjs';
import { MoviesService } from '@App/app/core/service/movies.service';
import { MovieDetailsResolver } from './movie-details.resolver';
import { MovieDetails } from '@App/app/shared/model/movie-details.model';
import { CreditsResponse } from '@App/app/shared/model/credits-response.model';
import { ReleaseDatesResponse } from '@App/app/shared/model/release-dates-response.model';
import { PopularMovieResponse } from '@App/app/shared/model/popular-movie-response.model';
import { VideosResponse } from '@App/app/shared/model/videos-response.models';

jest
  .mock('@angular/router')
  .mock('@App/app/core/service/movies.service')
  .mock('@App/app/shared/model/movie-details.model')
  .mock('@App/app/shared/model/credits-response.model')
  .mock('@App/app/shared/model/release-dates-response.model')
  .mock('@App/app/shared/model/popular-movie-response.model')
  .mock('@App/app/shared/model/videos-response.models');

describe('MovieDetailsResolver', () => {
  let resolver: MovieDetailsResolver;
  const mockState = jest.mocked({} as RouterStateSnapshot);
  const mockRoute = jest.mocked({} as ActivatedRouteSnapshot);
  let spyMovieDetails: SpyInstance<any>;
  let spyMovieCredits: SpyInstance<any>;
  let spyMovieReleaseDates: SpyInstance<any>;
  let spyMovieRecommendations: SpyInstance<any>;
  let spyMovieVideos: SpyInstance<any>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MoviesService
      ]
    });
    resolver = TestBed.inject(MovieDetailsResolver);
    jest.clearAllMocks();
    spyMovieDetails = jest.spyOn(MoviesService.prototype, 'getMovieDetails')
      .mockReturnValue(of(new MovieDetails()));
    spyMovieCredits = jest.spyOn(MoviesService.prototype, 'getMovieCredits')
      .mockReturnValue(of(new CreditsResponse()));
    spyMovieReleaseDates = jest.spyOn(MoviesService.prototype, 'getMovieReleaseDates')
      .mockReturnValue(of(new ReleaseDatesResponse()));
    spyMovieRecommendations = jest.spyOn(MoviesService.prototype, 'getMovieRecommendations')
      .mockReturnValue(of(new PopularMovieResponse()));
    spyMovieVideos = jest.spyOn(MoviesService.prototype, 'getMovieVideos')
      .mockReturnValue(of(new VideosResponse()));
  });

  test('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  test('should return EMPTY', () => {
    expect(resolver.resolve(mockRoute, mockState)).toBe(EMPTY);
  });

  test('should return call SpyMovieDetails and Fail', done => {
    spyMovieDetails.mockReturnValueOnce(
      throwError(() => new Error('MovieDetails Error'))
    );
    expect.assertions(1);
    callResolveAndSubscribe(false)
      .catch(error => {
        expect(error).toBeTruthy();
      })
      .finally(() => done());
  });
  test('should return call spyMovieCredits and Fail', done => {
    spyMovieCredits.mockReturnValueOnce(
      throwError(() => new Error('MovieCredits Error'))
    );
    expect.assertions(1);
    callResolveAndSubscribe(false)
      .catch((error) => expect(error).toBeTruthy())
      .finally(() => done());
  });

  test('should return call spyMovieReleaseDates and Fail', done => {
    spyMovieReleaseDates.mockReturnValueOnce(
      throwError(() => new Error('MovieReleaseDates Error'))
    );
    expect.assertions(1);
    callResolveAndSubscribe(false)
      .catch((error) => expect(error).toBeTruthy())
      .finally(() => done());
  });

  test('should return call spyMovieRecommendations and Fail', done => {
    spyMovieRecommendations.mockReturnValueOnce(
      throwError(() => new Error('MovieRecommendations Error'))
    );
    expect.assertions(1);
    callResolveAndSubscribe(false)
      .catch((error) => expect(error).toBeTruthy())
      .finally(() => done());
  });

  test('should return call spyMovieVideos and Fail', done => {
    spyMovieVideos.mockReturnValueOnce(
      throwError(() => new Error('MovieVideos Error'))
    );
    expect.assertions(1);
    callResolveAndSubscribe(false)
      .catch((error) => expect(error).toBeTruthy())
      .finally(() => done());
  });

  test('should return call all movie service functions', done => {
    callResolveAndSubscribe(false)
      .then(() => {
        expect(spyMovieDetails).toBeCalled();
        expect(spyMovieCredits).toBeCalled();
        expect(spyMovieReleaseDates).toBeCalled();
        expect(spyMovieRecommendations).toBeCalled();
        expect(spyMovieVideos).toBeCalled();
      })
      .catch((error) => fail(error))
      .finally(() => done());;
  });

  function callResolveAndSubscribe(showLog = false) {
    mockRoute.params = { id: 1 };
    
    return new Promise((success, reject) => {
      resolver.resolve(mockRoute, mockState).subscribe({
        next: value => {
          if (showLog) console.log(value);
          success(value);
        },
        error: error => {
          if (showLog) console.log(error);
          reject(error);
          throw new Error(error);
        },
        complete: () => {
          if (showLog) console.log("completed");
          success('');
        }
      });
    })
  } 
});
