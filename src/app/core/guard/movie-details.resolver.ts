import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  Params
} from '@angular/router';

import { CreditsResponse } from '@App/app/shared/model/credits-response.model';
import { MovieDetails } from '@App/app/shared/model/movie-details.model';
import { CompleteDetails } from '@App/app/shared/model/complete-details.model';
import { PopularMovieResponse } from '@App/app/shared/model/popular-movie-response.model';
import { ReleaseDatesResponse } from '@App/app/shared/model/release-dates-response.model';
import { VideosResponse } from '@App/app/shared/model/videos-response.models';
import { EMPTY, map, Observable, switchMap, tap, take, catchError, of } from 'rxjs';
import { MoviesService } from '../service/movies.service';

@Injectable({
  providedIn: 'root'
})
export class MovieDetailsResolver implements Resolve<CompleteDetails> {

  partialDetails = {} as Partial<CompleteDetails>;

  constructor(private service: MoviesService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CompleteDetails> {
    const params: Params = route.params;
    if (!params || !params['id']) {
      return EMPTY;
    }
    const movieId = params['id'];
    return this.service.getMovieDetails(movieId)
      .pipe(
        tap<MovieDetails>({ next: details => this.partialDetails.details = details })
      )
      .pipe(
        switchMap<any, Observable<CreditsResponse>>(
          _ => this.service.getMovieCredits(movieId)
          ),
        tap<CreditsResponse>({ next: credits => this.addCredits(credits) })
      )
      .pipe(
        switchMap<any, Observable<ReleaseDatesResponse>>(
          _ => this.service.getMovieReleaseDates(movieId)
        ),
        tap<ReleaseDatesResponse>({ next: dates => this.addReleaseDates(dates) })
      )
      .pipe(
        switchMap<any, Observable<PopularMovieResponse>>(
          _ => this.service.getMovieRecommendations(movieId)
        ),
        tap<PopularMovieResponse>({
          next: movies => this.partialDetails.recommendations = movies.results
        })
      )
      .pipe(
        switchMap<any, Observable<VideosResponse>>(
          _ => this.service.getMovieVideos(movieId)
        ),
        tap<VideosResponse>({
          next: videos => this.addVideos(videos)
        })
      )
      .pipe(
        map<any, CompleteDetails>(
          _ => this.partialDetails as CompleteDetails
        ),
        take(1)
      );
  }

  private addCredits(credits: CreditsResponse) {
    this.partialDetails.cast = credits.cast;
    this.partialDetails.crew = credits.crew;
  }

  private addVideos(videos: VideosResponse){
    this.partialDetails.videos = videos.results
      .filter(video => video.type.toLocaleLowerCase() == 'trailer')
      .slice(0, 1);
  }
  private addReleaseDates(dates: ReleaseDatesResponse) {
    this.partialDetails.releases = dates.results;
  }
}
