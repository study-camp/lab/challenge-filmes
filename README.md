# Challenge Filmes

Esta aplicação foi feita com o exercício para o uso de tecnologias de criação de interfaces para a web.

O projeto consiste de uma interface feita utilizando angular, para consumir a api TheMovieDB. 
A aplicação possui apenas duas páginas, a página principal que exibe uma lista dos filmes mais populares e uma página de detalhes.

Os requisitos levantados para a o projeto estão listados a seguir

* O usuário deve ter acesso a uma listagem dos filmes mais populares do dia
* O usuário deve conseguir paginar a lista para encontrar novos filmes
* O usuário deve ter acesso a uma outra página com detalhes sobre o filme, ao clicar em um item na listagem
* A página com detalhes de um filme deve possuir uma rota própria e estar preparada para ser indexada em mecanismos de pesquisa

## Tecnologias utilizadas
* O Projeto foi gerado com [Angular CLI](https://github.com/angular/angular-cli) 15.0.4.
* Angular Material: 15.0.4
* Node: 18.12.1
* NPM: 9.2.0
* CSS/SASS
* Typescrip: 4.8.4
* RxJS: 7.5.7
* JEST: 29.3

## Clonar repositório
    git clone https://gitlab.com/study-camp/lab/challenge-filmes.git
## Instalação
No terminal, na pasta raiz do projeto execute o comando 

    npm install
## Rodar a Aplicação localmente

Execute `ng serve` para ter acesso ao serivor de desenvolvimento. Navegue para `http://localhost:4200/`.

    ng serve
## Gerar build do projeto

Execute `ng build` para fazer o build do projeto. Os arquivos gerados serão colocados no diretório `dist/` directory.

    ng build

## Executar testes

Run `ng test` to execute the unit tests via [Jest](https://jestjs.io).

    ng test


